/**
 *   \file
 *   Class ConverterShineToNA61Tree
 *   \author V. Blinov, I. Selyuzhenkov, P. Parfenov, A. Grobov, V. Klochkov
 *   \date 01/01/2015
 */

#include "ConverterShineToNA61Tree.h"
#include <fwk/CentralConfig.h>
#include <utl/ErrorLogger.h>
#include <utl/ShineUnits.h>
#include <utl/UTCDateTime.h>
#include <utl/WithUnit.h>
#include <evt/Event.h>
#include <evt/RecEvent.h>
#include <evt/rec/RecEventConst.h>
#include <det/TriggerConst.h>
#include <evt/EventHeader.h>
#include <utl/TimeStamp.h>
#include <det/Detector.h>
#include <det/MagneticFieldTracker.h>
#include <det/TPC.h>
#include <det/PSD.h>
#include <evt/rec/BPDPlane.h>
#include <evt/rec/RecEventConst.h>
#include <det/BPDConst.h>

#include <TFile.h>
#include <TTree.h>
#include <TMath.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TDirectory.h>

#include <det/TPCConst.h>

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <algorithm>

using namespace fwk;
using namespace evt;
using namespace utl;
using namespace std;
using namespace evt;
using namespace evt::rec;
using namespace evt::sim;

namespace ConverterShineToNA61Tree {

    
    /// Init function for ConverterShineToNA61Tree
    VModule::EResultFlag ConverterShineToNA61Tree::Init()
    {
        
    CentralConfig& cc = CentralConfig::GetInstance();
    
    Branch topBranch = cc.GetTopBranch("ConverterShineToNA61Tree");
    InitVerbosity(topBranch);
    
    // Get the QA file name for output.
    topBranch.GetChild("ConverterShineToNA61TreeName").GetData(fNA61TreeFileName);
        TDirectory * tmpDir = (TDirectory*)gROOT->CurrentDirectory();
        std::cout << "ConverterShineToNA61Tree: fNA61TreeFileName: " << fNA61TreeFileName << std::endl;
        outputFile =  new TFile(fNA61TreeFileName.c_str(), "RECREATE");    
        std::cout << "ConverterShineToNA61Tree: outputFile name: " << outputFile->GetName() << std::endl;
        outputFile->cd();
        NA61Tree = new TTree("NA61Tree", "Selected NA61 data in a simple TTree format");
        //     NA61Tree->SetMaxTreeSize(90000000);
        Init_NA61Tree();
        tmpDir->cd();
        return eSuccess;
    }
    
    /// Process function for ConverterShineToNA61Tree
    VModule::EResultFlag
    ConverterShineToNA61Tree::Process(Event& event, const AttributeMap& /*attr*/)
    {
        //       std::cout << "ClearEvent" << std::endl;
        ClearEvent();
        //       std::cout << "Convert" << std::endl;
        Convert(event);
        return eSuccess;
    }
    
    /// Finish function for ConverterShineToNA61Tree
    VModule::EResultFlag
    ConverterShineToNA61Tree::Finish()
    {
        outputFile->cd();
        NA61Tree -> Write();
        //     outputFile->Close();
        TDirectory * tmpDir = (TDirectory*)gROOT->CurrentDirectory();
        tmpDir -> cd();
        return eSuccess;
    }
    
    void ConverterShineToNA61Tree::Convert(Event& event)
    {
        cout << "ConverterShineToNA61Tree::Convert" << endl;
        //Event
        const EventHeader& evtHeader = event.GetEventHeader();
        EventInfo_Event_Timestamp = evtHeader.GetTime().GetGPSSecond();
        EventInfo_Event_Id = evtHeader.GetId();
        EventInfo_Run_Id = evtHeader.GetRunNumber();
        const det::Detector& detector = det::Detector::GetInstance();
        //detector.Update(EventInfo_Event_Timestamp,EventInfo_Run_Id);
        
        const det::MagneticFieldTracker& tracker = detector.GetMagneticFieldTracker();
        
        //SimEvent
        using namespace evt::sim;
        const SimEvent& simEvent = event.GetSimEvent();
        if (event.IsSimulation())
        {
            cout << "ConverterShineToNA61Tree::Convert //SimEvent" << endl;
            //Event
            Sim_ImpactParameter = simEvent.GetPrimaryInteraction().GetImpactParameter();
            Sim_ReactionPlaneAngle = simEvent.GetPrimaryInteraction().GetEventPlaneAngle();
            //Tracks
            Sim_nTracks = 0;
            int counter = 0;
            if (simEvent.HasMainVertex())
            {
                for (std::list<evt::sim::VertexTrack>::const_iterator simTrackIter = simEvent.Begin<evt::sim::VertexTrack>(), simTrackEnd = simEvent.End<evt::sim::VertexTrack>(); simTrackIter != simTrackEnd; ++simTrackIter)
                {
                    
                    cout << "ConverterShineToNA61Tree::Convert //SimEvent counter = " << counter << endl;
                    counter++;
                    //                     const evt::sim::VertexTrack& simTrack = simEvent.Get(*vtxTrackIter);
                    const evt::sim::VertexTrack& simTrack = *simTrackIter;
                    const Vector& momentum = simTrack.GetMomentum();
                    Sim_track_pT[counter] = momentum.GetRho();
                    Sim_track_phi[counter] = momentum.GetPhi();
                    Sim_track_eta[counter] = 0.5*TMath::Log((momentum.GetR()+momentum.GetZ())/(momentum.GetR()-momentum.GetZ()));
                    //                     Sim_track_mass[counter] = ???;
                    Sim_track_charge[counter] = simTrack.GetCharge();
                    Sim_track_pdg_id[counter] = simTrack.GetParticleId();
                    Sim_track_IsInTPC_Total[counter] = (simTrack.IsInTPC(det::TPCConst::eVTPC1)+simTrack.IsInTPC(det::TPCConst::eVTPC2)+simTrack.IsInTPC(det::TPCConst::eMTPCL)+simTrack.IsInTPC(det::TPCConst::eMTPCR) > 0);
                    Sim_track_IsInTPC_TPCV1[counter] = simTrack.IsInTPC(det::TPCConst::eVTPC1);
                    Sim_track_IsInTPC_TPCV2[counter] = simTrack.IsInTPC(det::TPCConst::eVTPC2);
                    Sim_track_IsInTPC_TPCVmain[counter] = (simTrack.IsInTPC(det::TPCConst::eMTPCL)+simTrack.IsInTPC(det::TPCConst::eMTPCR) > 0);
                    
                    Sim_track_Number_of_Hits[counter] = simTrack.GetNumberOfHits();
                    Sim_track_IsSpectator[counter] = simTrack.IsSpectator();
                    Sim_nTracks++;
                    //         bool Sim_track_IsInTPC[nMaxTracks];
                }
            }
            
            //PSD
            evt::sim::PSD& simPSD = event.GetSimEvent().GetPSD();
            for (int i=0;i<int(simPSD.GetNModules());i++)
            {
                evt::sim::PSDModule simPSDModule = simPSD.GetModule(i+1);
                Sim_PSD_module_Temperature[i] = simPSDModule.GetTemperature();
                for (int j=0;j<simPSDModule.GetNSections();j++)
                {
                    //                     cout << "mod " << i << " sec " << j << endl;
                    evt::sim::PSDSection simPSDSecection = simPSDModule.GetSection(j+1);
                    Sim_PSD_Energy += simPSDSecection.GetEnergy();
                    Sim_PSD_module_Energy[i] += simPSDSecection.GetEnergy();
                    Sim_PSD_section_Energy[i][j] = simPSDSecection.GetEnergy();
                }
            }
        }
        //RecEvent
        const evt::RecEvent& recEvent = event.GetRecEvent();
        if (!event.IsSimulation())
        {
            cout << "ConverterShineToNA61Tree::Convert //RecEvent" << endl;
            //Main vertex
            const evt::rec::Vertex& mainVertex = recEvent.GetMainVertex();
            const Point& pm = mainVertex.GetPosition();
            Main_Vertex_X = pm.GetX(); //Main vertex = Data Base vertex (DBVtx). It should be renamed properly.
            Main_Vertex_Y = pm.GetY();
            Main_Vertex_Z = pm.GetZ();
            if (event.GetRecEvent().HasPrimaryVertex(rec::VertexConst::ePrimaryFitZ)){
                const evt::rec::Vertex& pVtx = recEvent.GetPrimaryVertex(rec::VertexConst::ePrimaryFitZ);
                const Point& pntVtx = pVtx.GetPosition();
                pVtx_Z = pntVtx.GetZ();
            } else{
                pVtx_Z = -999;
            } 
            //TPC
            float px;
            float py;
            float pz;
            float p;
            float pT;
            float eta;
            float phi;
//             int charge;
            Point endPos, endPosFirst, endPosLast, endTestPos, endTestPosFirst, endTestPosLast, endPSDPos;
            Vector endP, endPFirst, endPLast, endTestP, endTestPFirst, endTestPLast, endPSDP;
            const float PSD_Z = 17000.; 
            
            //Main vertex tracks
            int track_id = 0;
            Track_DBVtx_nTracks = 0;
            std::cout<<"BEGIN: evt::rec::VertexTrackIndexIterator vtxTrackIter"<<std::endl;
            for (evt::rec::VertexTrackIndexIterator vtxTrackIter = mainVertex.DaughterTracksBegin(); vtxTrackIter != mainVertex.DaughterTracksEnd(); ++vtxTrackIter)
            {
                TPC_Multiplicity_all++;
                const evt::rec::VertexTrack& vtxTrack = recEvent.Get(*vtxTrackIter);
                if (! (vtxTrack.GetStatus() == 0 && vtxTrack.HasTrack())) continue;
                TPC_Multiplicity++;
                //main vtx track (momentum of track from main vertex (for pA reconstruciton this is BPD vertex, nominal) at the vertex point)
                px=vtxTrack.GetMomentum().GetX();
                py=vtxTrack.GetMomentum().GetY();
                pz=vtxTrack.GetMomentum().GetZ();
                p=TMath::Sqrt(px*px+py*py+pz*pz);
                pT=TMath::Sqrt(px*px+py*py);
                eta = 0.5*TMath::Log((p+pz)/(p-pz));
                phi = TMath::ATan2(py,px);
                if (TMath::IsNaN(eta)) { cout << "ERROR NA61Tree: eta is NaN!" << endl; continue; }
                if (TMath::IsNaN(phi)) { cout << "ERROR NA61Tree: phi is NaN!" << endl; continue; }
                Track_DBVtx_pT[track_id] = pT;
                Track_DBVtx_px[track_id] = px;
                Track_DBVtx_py[track_id] = py;
                Track_DBVtx_pz[track_id] = pz;
                Track_DBVtx_eta[track_id] = eta;
                Track_DBVtx_phi[track_id] = phi;
                //get tracks in TPC associated with the main vertex
                const evt::rec::Track& track = recEvent.Get(vtxTrack.GetTrackIndex());
                using namespace TrackConst;
                //track momentum at first point in TPC (track from main vertex)
                px = track.GetMomentum().GetX();
                py = track.GetMomentum().GetY();
                pz = track.GetMomentum().GetZ();
                p=TMath::Sqrt(px*px+py*py+pz*pz);
                pT=TMath::Sqrt(px*px+py*py);
                eta = 0.5*TMath::Log((p+pz)/(p-pz));
                phi = TMath::ATan2(py,px);
                if (TMath::IsNaN(eta)) { cout << "ERROR NA61Tree: eta is NaN!" << endl; continue; }
                if (TMath::IsNaN(phi)) { cout << "ERROR NA61Tree: phi is NaN!" << endl; continue; }
                Track_DBVtx_track_pT[track_id] = pT;
                Track_DBVtx_track_px[track_id] = px;
                Track_DBVtx_track_py[track_id] = py;
                Track_DBVtx_track_pz[track_id] = pz;
                Track_DBVtx_track_eta[track_id] = eta;
                Track_DBVtx_track_phi[track_id] = phi;
                Track_DBVtx_track_point_x[track_id] = track.GetMomentumPoint().GetX();
                Track_DBVtx_track_point_y[track_id] = track.GetMomentumPoint().GetY();
                Track_DBVtx_track_point_z[track_id] = track.GetMomentumPoint().GetZ();
                Track_DBVtx_track_first_point_x[track_id] = track.GetFirstPointOnTrack().GetX();
                Track_DBVtx_track_first_point_y[track_id] = track.GetFirstPointOnTrack().GetY();
                Track_DBVtx_track_first_point_z[track_id] = track.GetFirstPointOnTrack().GetZ();
                Track_DBVtx_track_last_point_x[track_id] = track.GetLastPointOnTrack().GetX();
                Track_DBVtx_track_last_point_y[track_id] = track.GetLastPointOnTrack().GetY();
                Track_DBVtx_track_last_point_z[track_id] = track.GetLastPointOnTrack().GetZ();
                

                                
                //Track Parameters
                
                const utl::TrackParameters trackParameters = recEvent.GetTrackParameters(vtxTrack.GetTrackIndex());
                std::vector< float > 	trackParametersValues = trackParameters.GetParameters();
//                  std::cout<< trackParametersValues.size()<<" VECTOR SIZE!!! "       <<std::endl;
                trackParameters_qOverPXZ[track_id] = trackParametersValues.at(0);
                trackParameters_tanLambda[track_id] =trackParametersValues.at(1);
                trackParameters_phi[track_id] =trackParametersValues.at(2);
                trackParameters_vtx_PositionX[track_id] =trackParametersValues.at(3);
                trackParameters_vtx_PositionY[track_id] =trackParametersValues.at(4);
//                 trackParameters_vtx_PositionZ[track_id] =trackParametersValues.at(5);
//                 trackParameters_Charge[track_id] = trackParametersValues.at(6);
//                 std::cout<< trackParameters_qOverPXZ[track_id]<<" "     <<std::endl;
//                 std::cout<< trackParameters_tanLambda[track_id]<<" "    <<std::endl;
//                 std::cout<< trackParameters_phi[track_id]<<" "          <<std::endl;
//                 std::cout<< trackParameters_vtx_PositionX[track_id]<<" "<<std::endl;
//                 std::cout<< trackParameters_vtx_PositionY[track_id]<<" "<<std::endl;
//                 std::cout<< trackParameters_vtx_PositionZ[track_id]<<" "<<std::endl;
//                 std::cout<< trackParameters_Charge[track_id]<<" "       <<std::endl;
                
                
                //Covariance Matrix
                
                const utl::CovarianceMatrix fCovarianceMatrix = vtxTrack.GetCovarianceMatrix(); // See https://shinedoc.web.cern.ch/shinedoc/doxygen/v1r5p0/RecEvent_2VertexTrack_8h_source.html. the parameters are in the NA61 representation (q/pxz, tan(lambda), psi, x, y). See also https://shinedoc.web.cern.ch/shinedoc/doxygen/v1r5p0/classutl_1_1TrackParameters.html
                const int nTrackParametersCheck= utl::TrackParameters::eNTrackParameters;
                
                if(nTrackParameters==nTrackParametersCheck){

                for(int i=0; i<nTrackParameters; i++) {
                    for(int j =0; j<nTrackParameters; j++) {
                            CovarianceMatrixArray[track_id][i][j]=fCovarianceMatrix[i][j];
//                             std::cout<< CovarianceMatrixArray[track_id][i][j]<<" ";
                    }
//                     std::cout<<std::endl;
                }
                } else{
                std::cout<<"THERE IS A MISTAKE: NUMBER OF TRACK PARAMETERS DOESN'T MATCH THE ACTUAL VALUE!"<<std::endl;
                
                }

                
                if (track.GetMomentumPoint().GetZ()) tracker.TrackToZ(track.GetMomentumPoint().GetZ(), track.GetCharge(), vtxTrack.GetImpactPoint(), vtxTrack.GetMomentum(), endTestPos, endTestP);
                if (track.GetFirstPointOnTrack().GetZ()) tracker.TrackToZ(track.GetFirstPointOnTrack().GetZ(), track.GetCharge(), vtxTrack.GetImpactPoint(), vtxTrack.GetMomentum(), endTestPosFirst, endTestPFirst);
                if (track.GetLastPointOnTrack().GetZ()) tracker.TrackToZ(track.GetLastPointOnTrack().GetZ(), track.GetCharge(), vtxTrack.GetImpactPoint(), vtxTrack.GetMomentum(), endTestPosLast, endTestPLast);
                //if (pVtx_Z == -999) pVtx_Z = vtxTrack.GetImpactPoint().GetZ();
                if (pVtx_Z != -999){
                    if (track.GetMomentumPoint().GetZ()) tracker.TrackToZ(pVtx_Z, vtxTrack.GetCharge(), track.GetMomentumPoint(), track.GetMomentum(), endPos, endP);
                    if (track.GetFirstPointOnTrack().GetZ()) tracker.TrackToZ(pVtx_Z, vtxTrack.GetCharge(), track.GetFirstPointOnTrack(), track.GetMomentum(), endPosFirst, endPFirst);
                    if (track.GetLastPointOnTrack().GetZ()) tracker.TrackToZ(pVtx_Z, vtxTrack.GetCharge(), track.GetLastPointOnTrack(), track.GetMomentum(), endPosLast, endPLast);
                }
                
                tracker.TrackToZ(PSD_Z, track.GetCharge(), vtxTrack.GetImpactPoint(), vtxTrack.GetMomentum(), endPSDPos, endPSDP);
                
                Track_DBVtx_impact_point_X[track_id] = vtxTrack.GetImpactPoint().GetX();
                Track_DBVtx_impact_point_Y[track_id] = vtxTrack.GetImpactPoint().GetY();
                Track_DBVtx_impact_point_Z[track_id] = vtxTrack.GetImpactPoint().GetZ();
                Track_extDBVtx_track_px[track_id] = endP.GetX();
                Track_extDBVtx_track_py[track_id] = endP.GetY();
                Track_extDBVtx_track_pz[track_id] = endP.GetZ();
                Track_extDBVtx_first_track_px[track_id] = endPFirst.GetX();
                Track_extDBVtx_first_track_py[track_id] = endPFirst.GetY();
                Track_extDBVtx_first_track_pz[track_id] = endPFirst.GetZ();
                Track_extDBVtx_last_track_px[track_id] = endPLast.GetX();
                Track_extDBVtx_last_track_py[track_id] = endPLast.GetY();
                Track_extDBVtx_last_track_pz[track_id] = endPLast.GetZ();
                Track_extDBVtx_point_x[track_id] = endPos.GetX();
                Track_extDBVtx_point_y[track_id] = endPos.GetY();
                Track_extDBVtx_point_z[track_id] = endPos.GetZ();
                Track_extDBVtx_first_point_x[track_id] = endPosFirst.GetX();
                Track_extDBVtx_first_point_y[track_id] = endPosFirst.GetY();
                Track_extDBVtx_first_point_z[track_id] = endPosFirst.GetZ();
                Track_extDBVtx_last_point_x[track_id] = endPosLast.GetX();
                Track_extDBVtx_last_point_y[track_id] = endPosLast.GetY();
                Track_extDBVtx_last_point_z[track_id] = endPosLast.GetZ();
                
                Track_extDB_test_track_px[track_id] = endTestP.GetX();
                Track_extDB_test_track_py[track_id] = endTestP.GetY();
                Track_extDB_test_track_pz[track_id] = endTestP.GetZ();
                Track_extDB_test_first_track_px[track_id] = endTestPFirst.GetX();
                Track_extDB_test_first_track_py[track_id] = endTestPFirst.GetY();
                Track_extDB_test_first_track_pz[track_id] = endTestPFirst.GetZ();
                Track_extDB_test_last_track_px[track_id] = endTestPLast.GetX();
                Track_extDB_test_last_track_py[track_id] = endTestPLast.GetY();
                Track_extDB_test_last_track_pz[track_id] = endTestPLast.GetZ();
                Track_extDB_test_point_x[track_id] = endTestPos.GetX();
                Track_extDB_test_point_y[track_id] = endTestPos.GetY();
                Track_extDB_test_point_z[track_id] = endTestPos.GetZ();
                Track_extDB_test_first_point_x[track_id] = endTestPosFirst.GetX();
                Track_extDB_test_first_point_y[track_id] = endTestPosFirst.GetY();
                Track_extDB_test_first_point_z[track_id] = endTestPosFirst.GetZ();
                Track_extDB_test_last_point_x[track_id] = endTestPosLast.GetX();
                Track_extDB_test_last_point_y[track_id] = endTestPosLast.GetY();
                Track_extDB_test_last_point_z[track_id] = endTestPosLast.GetZ();
                
                Track_extDB_PSD_track_px[track_id] = endPSDP.GetX();
                Track_extDB_PSD_track_py[track_id] = endPSDP.GetY();
                Track_extDB_PSD_track_pz[track_id] = endPSDP.GetZ();
                Track_extDB_PSD_point_x[track_id] = endPSDPos.GetX();
                Track_extDB_PSD_point_y[track_id] = endPSDPos.GetY();
                Track_extDB_PSD_point_z[track_id] = endPSDPos.GetZ();
                
                
                //track TPC parameters
                Track_DBVtx_track_charge[track_id] = track.GetCharge();
                Track_DBVtx_track_nClusters_Total[track_id] = track.GetNumberOfClusters(eAll);
                Track_DBVtx_track_nClusters_TPCV1[track_id] = track.GetNumberOfClusters(eVTPC1);
                Track_DBVtx_track_nClusters_TPCV2[track_id] = track.GetNumberOfClusters(eVTPC2);
                Track_DBVtx_track_nClusters_TPCVmain[track_id] = track.GetNumberOfClusters(eMTPC);
                Track_DBVtx_track_nClusters_TPCVgap[track_id] = track.GetNumberOfClusters(eGTPC);
                Track_DBVtx_track_nClustersPotential_Total[track_id] = track.GetPotentialNumberOfClusters(eAll);
                Track_DBVtx_track_nClustersPotential_TPCV1[track_id] = track.GetPotentialNumberOfClusters(eVTPC1);
                Track_DBVtx_track_nClustersPotential_TPCV2[track_id] = track.GetPotentialNumberOfClusters(eVTPC2);
                Track_DBVtx_track_nClustersPotential_TPCVmain[track_id] = track.GetPotentialNumberOfClusters(eMTPC);
                Track_DBVtx_track_nClustersPotential_TPCVgap[track_id] = track.GetPotentialNumberOfClusters(eGTPC);
                Track_DBVtx_track_nClustersFit_Total[track_id] = track.GetNumberOfFitClusters(eAll);
                Track_DBVtx_track_nClustersFit_TPCV1[track_id] = track.GetNumberOfFitClusters(eVTPC1);
                Track_DBVtx_track_nClustersFit_TPCV2[track_id] = track.GetNumberOfFitClusters(eVTPC2);
                Track_DBVtx_track_nClustersFit_TPCVmain[track_id] = track.GetNumberOfFitClusters(eMTPC);
                Track_DBVtx_track_nClustersFit_TPCVgap[track_id] = track.GetNumberOfFitClusters(eGTPC);
                Track_DBVtx_track_nClustersdEdX_Total[track_id] = track.GetNumberOfdEdXClusters(eAll);
                Track_DBVtx_track_nClustersdEdX_TPCV1[track_id] = track.GetNumberOfdEdXClusters(eVTPC1);
                Track_DBVtx_track_nClustersdEdX_TPCV2[track_id] = track.GetNumberOfdEdXClusters(eVTPC2);
                Track_DBVtx_track_nClustersdEdX_TPCVmain[track_id] = track.GetNumberOfdEdXClusters(eMTPC);
                Track_DBVtx_track_nClustersdEdX_TPCVgap[track_id] = track.GetNumberOfdEdXClusters(eGTPC);
                Track_DBVtx_track_EnergyClusters_Total[track_id] = track.GetEnergyDeposit(eAll);
                Track_DBVtx_track_EnergyClusters_TPCV1[track_id] = track.GetEnergyDeposit(eVTPC1);
                Track_DBVtx_track_EnergyClusters_TPCV2[track_id] = track.GetEnergyDeposit(eVTPC2);
                Track_DBVtx_track_EnergyClusters_TPCVmain[track_id] = track.GetEnergyDeposit(eMTPC);
                Track_DBVtx_track_EnergyClusters_TPCVgap[track_id] = track.GetEnergyDeposit(eGTPC);
                Track_DBVtx_track_chi2[track_id] = vtxTrack.GetChi2();
                Track_DBVtx_track_ndf[track_id] = vtxTrack.GetNdf();
                track_id++;
                Track_DBVtx_nTracks++;
                if (track_id > nMaxTracks)
                {
                    for (int k=0;k<nMaxTracks;k++)
                    {
                        Track_DBVtx_track_pT[k] = undefinedValue;
                    }
                }
            }
            
            Has_Primary_Vertex = false;    
            if (event.GetRecEvent().HasPrimaryVertex(rec::VertexConst::ePrimaryFitZ))
            {
                //Primary vertex
                Has_Primary_Vertex = true;
                const evt::rec::Vertex& primaryVertex = recEvent.GetPrimaryVertex(rec::VertexConst::ePrimaryFitZ);
                const Point& pm = primaryVertex.GetPosition();
                Primary_Vertex_X = pm.GetX(); //Primary vertex = Reconstructed vertex (RecoVtx). It should be renamed properly.
                Primary_Vertex_Y = pm.GetY();
                Primary_Vertex_Z = pm.GetZ();
                Primary_Vertex_Q = (int) primaryVertex.GetFitQuality();
                //Primary vertex tracks
                int track_id = 0;
                Track_RecoVtx_nTracks = 0;
                for (evt::rec::VertexTrackIndexIterator vtxTrackIter = primaryVertex.DaughterTracksBegin(); vtxTrackIter != primaryVertex.DaughterTracksEnd(); ++vtxTrackIter)
                {
                    const evt::rec::VertexTrack& vtxTrack = recEvent.Get(*vtxTrackIter);
                    if (! (vtxTrack.GetStatus() == 0 && vtxTrack.HasTrack())) continue;
                    //priamry vtx track (momentum of track from primary vertex (fitted vertex position) at the vertex point)
                    px=vtxTrack.GetMomentum().GetX();
                    py=vtxTrack.GetMomentum().GetY();
                    pz=vtxTrack.GetMomentum().GetZ();
                    p=TMath::Sqrt(px*px+py*py+pz*pz);
                    pT=TMath::Sqrt(px*px+py*py);
                    eta = 0.5*TMath::Log((p+pz)/(p-pz));
                    phi = TMath::ATan2(py,px);
                    if (TMath::IsNaN(eta)) { cout << "ERROR NA61Tree: eta is NaN!" << endl; continue; }
                    if (TMath::IsNaN(phi)) { cout << "ERROR NA61Tree: phi is NaN!" << endl; continue; }
                    Track_RecoVtx_pT[track_id] = pT;
                    Track_RecoVtx_eta[track_id] = eta;
                    Track_RecoVtx_phi[track_id] = phi;
                    //get tracks in TPC associated with the primary vertex
                    const evt::rec::Track& track = recEvent.Get(vtxTrack.GetTrackIndex());
                    using namespace TrackConst;
                    //track momentum at first point in TPC (track from primary vertex)
                    px = track.GetMomentum().GetX();
                    py = track.GetMomentum().GetY();
                    pz = track.GetMomentum().GetZ();
                    p=TMath::Sqrt(px*px+py*py+pz*pz);
                    pT=TMath::Sqrt(px*px+py*py);
                    eta = 0.5*TMath::Log((p+pz)/(p-pz));
                    phi = TMath::ATan2(py,px);
                    if (TMath::IsNaN(eta)) { cout << "ERROR NA61Tree: eta is NaN!" << endl; continue; }
                    if (TMath::IsNaN(phi)) { cout << "ERROR NA61Tree: phi is NaN!" << endl; continue; }
                    Track_RecoVtx_track_pT[track_id] = pT;
                    Track_RecoVtx_track_eta[track_id] = eta;
                    Track_RecoVtx_track_phi[track_id] = phi;
                    Track_RecoVtx_impact_point_X[track_id] = vtxTrack.GetImpactPoint().GetX();
                    Track_RecoVtx_impact_point_Y[track_id] = vtxTrack.GetImpactPoint().GetY();
                    Track_RecoVtx_impact_point_Z[track_id] = vtxTrack.GetImpactPoint().GetZ();
                    
                    //track TPC parameters
                    Track_RecoVtx_track_charge[track_id] = track.GetCharge();
                    Track_RecoVtx_track_nClusters_Total[track_id] = track.GetNumberOfClusters(eAll);
                    Track_RecoVtx_track_nClusters_TPCV1[track_id] = track.GetNumberOfClusters(eVTPC1);
                    Track_RecoVtx_track_nClusters_TPCV2[track_id] = track.GetNumberOfClusters(eVTPC2);
                    Track_RecoVtx_track_nClusters_TPCVmain[track_id] = track.GetNumberOfClusters(eMTPC);
                    Track_RecoVtx_track_nClusters_TPCVgap[track_id] = track.GetNumberOfClusters(eGTPC);
                    Track_RecoVtx_track_nClustersPotential_Total[track_id] = track.GetPotentialNumberOfClusters(eAll);
                    Track_RecoVtx_track_nClustersPotential_TPCV1[track_id] = track.GetPotentialNumberOfClusters(eVTPC1);
                    Track_RecoVtx_track_nClustersPotential_TPCV2[track_id] = track.GetPotentialNumberOfClusters(eVTPC2);
                    Track_RecoVtx_track_nClustersPotential_TPCVmain[track_id] = track.GetPotentialNumberOfClusters(eMTPC);
                    Track_RecoVtx_track_nClustersPotential_TPCVgap[track_id] = track.GetPotentialNumberOfClusters(eGTPC);
                    Track_RecoVtx_track_nClustersFit_Total[track_id] = track.GetNumberOfFitClusters(eAll);
                    Track_RecoVtx_track_nClustersFit_TPCV1[track_id] = track.GetNumberOfFitClusters(eVTPC1);
                    Track_RecoVtx_track_nClustersFit_TPCV2[track_id] = track.GetNumberOfFitClusters(eVTPC2);
                    Track_RecoVtx_track_nClustersFit_TPCVmain[track_id] = track.GetNumberOfFitClusters(eMTPC);
                    Track_RecoVtx_track_nClustersFit_TPCVgap[track_id] = track.GetNumberOfFitClusters(eGTPC);
                    Track_RecoVtx_track_nClustersdEdX_Total[track_id] = track.GetNumberOfdEdXClusters(eAll);
                    Track_RecoVtx_track_nClustersdEdX_TPCV1[track_id] = track.GetNumberOfdEdXClusters(eVTPC1);
                    Track_RecoVtx_track_nClustersdEdX_TPCV2[track_id] = track.GetNumberOfdEdXClusters(eVTPC2);
                    Track_RecoVtx_track_nClustersdEdX_TPCVmain[track_id] = track.GetNumberOfdEdXClusters(eMTPC);
                    Track_RecoVtx_track_nClustersdEdX_TPCVgap[track_id] = track.GetNumberOfdEdXClusters(eGTPC);
                    Track_RecoVtx_track_EnergyClusters_Total[track_id] = track.GetEnergyDeposit(eAll);
                    Track_RecoVtx_track_EnergyClusters_TPCV1[track_id] = track.GetEnergyDeposit(eVTPC1);
                    Track_RecoVtx_track_EnergyClusters_TPCV2[track_id] = track.GetEnergyDeposit(eVTPC2);
                    Track_RecoVtx_track_EnergyClusters_TPCVmain[track_id] = track.GetEnergyDeposit(eMTPC);
                    Track_RecoVtx_track_EnergyClusters_TPCVgap[track_id] = track.GetEnergyDeposit(eGTPC);
                    Track_RecoVtx_track_chi2[track_id] = vtxTrack.GetChi2();
                    Track_RecoVtx_track_ndf[track_id] = vtxTrack.GetNdf();
                    track_id++;
                    Track_RecoVtx_nTracks++;
                    if (track_id > nMaxTracks)
                    {
                        std::cout << "Too many tracks!" << std::endl;
                        for (int k=0;k<nMaxTracks;k++)
                        {
                            Track_RecoVtx_track_pT[k] = undefinedValue;
                        }
                    }
                }
            }
            //PSD
            const evt::rec::PSD& recPSDEvent = recEvent.GetPSD();
            for (int i=0; i<(int)recPSDEvent.GetNModules(); i++)
            {
                evt::rec::PSDModule fPSDmodule = recPSDEvent.GetModule(i+1);
                PSD_module_number_of_sections[i] = fPSDmodule.GetNSections();
                PSD_module_X[i] = detector.GetPSD().GetPSDModule(det::PSDConst::GetType(det::PSDConst::GetIdByNumber(i+1)))->GetModulePositionX(det::PSDConst::GetIdByNumber(i+1));
                PSD_module_Y[i] = detector.GetPSD().GetPSDModule(det::PSDConst::GetType(det::PSDConst::GetIdByNumber(i+1)))->GetModulePositionY(det::PSDConst::GetIdByNumber(i+1));
                for (int j=0; j<(int)fPSDmodule.GetNSections(); j++)
                {
                    evt::rec::PSDSection fPSDsection = fPSDmodule.GetSection(j+1);
                    PSD_section_Energy[i][j] = fPSDsection.GetEnergy();
                    if (i!=44) PSD_section_slice_Energy[j] += PSD_section_Energy[i][j];
                    PSD_module_Energy[i] += fPSDsection.GetEnergy();
                    if (PSD_section_Energy[i][j] > 0) {PSD_section_Number++;PSD_section_Number_array[i][j]++;}
                }
                if (PSD_module_Energy[i] > 0) {PSD_module_Number++;PSD_module_Number_array[i]++;}
                if (i<16 || i==44) PSD_1_Energy += PSD_module_Energy[i];
                if (i>=16 && i<28) PSD_2_Energy += PSD_module_Energy[i];
                if (i>=28 && i<44) PSD_3_Energy += PSD_module_Energy[i];
                PSD_Energy += PSD_module_Energy[i];
            }
        }
        
        
        //BEGIN TEST
        //Triggers
        
        if (!event.IsSimulation())
        {
            evt::rec::BPDPlane fBPDPlaneReco[nBPD][nBPDcomponents];
            fBPDPlaneReco[0][0] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD1, det::BPDConst::eX);
            fBPDPlaneReco[0][1] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD1, det::BPDConst::eY);
            fBPDPlaneReco[1][0] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD2, det::BPDConst::eX);
            fBPDPlaneReco[1][1] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD2, det::BPDConst::eY);
            fBPDPlaneReco[2][0] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD3, det::BPDConst::eX);
            fBPDPlaneReco[2][1] = event.GetRecEvent().GetBeam().GetBPDPlane(det::BPDConst::eBPD3, det::BPDConst::eY);
            
            
            for (int i=0;i<nBPD;i++)
            {
                for (int j=0;j<nBPDcomponents;j++)
                {
                    
                    BPD_Status[1][i][j] = fBPDPlaneReco[i][j].GetStatus();
                    BPD_Position[1][i][j] = fBPDPlaneReco[i][j].GetPosition();
                    BPD_PositionError[1][i][j] = fBPDPlaneReco[i][j].GetPositionError();
                    BPD_Z[1][i][j] = fBPDPlaneReco[i][j].GetZ();
                    BPD_RMS[1][i][j] = fBPDPlaneReco[i][j].GetRMS();
                    BPD_Maximum[1][i][j] = fBPDPlaneReco[i][j].GetMaximum();
                    BPD_Charge[1][i][j] = fBPDPlaneReco[i][j].GetCharge();
                    BPD_SumOfAll[1][i][j] = fBPDPlaneReco[i][j].GetSumOfAll();
                }
            }

        }
        evt::rec::Trigger fTriggerRec = event.GetRecEvent().GetBeam().GetTrigger();
        evt::raw::Trigger fTriggerRaw = event.GetRawEvent().GetBeam().GetTrigger();

        
        triggersADC[0][0] = fTriggerRaw.GetADC(det::TriggerConst::eS1);
        triggersADC[0][1] = fTriggerRaw.GetADC(det::TriggerConst::eS2);
        triggersADC[0][2] = fTriggerRaw.GetADC(det::TriggerConst::eS3);
        triggersADC[0][3] = fTriggerRaw.GetADC(det::TriggerConst::eV1);
        triggersADC[0][4] = fTriggerRaw.GetADC(det::TriggerConst::eV1p);
        triggersADC[0][5] = fTriggerRaw.GetADC(det::TriggerConst::ePSD);
        
        isTriggers_Simple[0][0] = fTriggerRaw.IsTrigger(det::TriggerConst::eS1,det::TriggerConst::ePrescaled);
        isTriggers_Simple[0][1] = fTriggerRaw.IsTrigger(det::TriggerConst::eS2,det::TriggerConst::ePrescaled);
        isTriggers_Simple[0][2] = fTriggerRaw.IsTrigger(det::TriggerConst::eS3,det::TriggerConst::ePrescaled);
        isTriggers_Simple[0][3] = fTriggerRaw.IsTrigger(det::TriggerConst::eV1,det::TriggerConst::ePrescaled);
        isTriggers_Simple[0][4] = fTriggerRaw.IsTrigger(det::TriggerConst::eV1p,det::TriggerConst::ePrescaled);
        isTriggers_Simple[0][5] = fTriggerRaw.IsTrigger(det::TriggerConst::ePSD,det::TriggerConst::ePrescaled);
        
        isTriggers_Combined[0][0] = fTriggerRaw.IsTrigger(det::TriggerConst::eT1,det::TriggerConst::ePrescaled);
        isTriggers_Combined[0][1] = fTriggerRaw.IsTrigger(det::TriggerConst::eT2,det::TriggerConst::ePrescaled);
        isTriggers_Combined[0][2] = fTriggerRaw.IsTrigger(det::TriggerConst::eT3,det::TriggerConst::ePrescaled);
        isTriggers_Combined[0][3] = fTriggerRaw.IsTrigger(det::TriggerConst::eT4,det::TriggerConst::ePrescaled);
        
        if (!event.IsSimulation())
        {
            Beam_Momentum[1][0] = event.GetRecEvent().GetBeam().GetMomentum().GetX();
            Beam_Momentum[1][1] = event.GetRecEvent().GetBeam().GetMomentum().GetY();
            Beam_Momentum[1][2] = event.GetRecEvent().GetBeam().GetMomentum().GetZ();
            Beam_Status[1] = event.GetRecEvent().GetBeam().GetStatus();
        }
        //END TEST
        
        if (!event.IsSimulation())
        {
            if (fTriggerRaw.HasTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eS1_1))
            {
                WFA_NumberOfSignalHits[0] = fTriggerRaw.GetNumberOfSignalHits(det::TimeStructureConst::eWFA,det::TriggerConst::eS1_1);
                for (int j=0;j<nMaxWFAsignals;j++)
                {
                    WFA_TimeStructure[0][j] = undefinedValue;
                }
                for (int j=0;j<int(fTriggerRaw.GetTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eS1_1).size());j++)
                {
                    WFA_TimeStructure[0][j] = fTriggerRaw.GetTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eS1_1).at(j);
                }
            }

            if (fTriggerRaw.HasTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eT4))
            {
                WFA_NumberOfSignalHits[1] = fTriggerRaw.GetNumberOfSignalHits(det::TimeStructureConst::eWFA,det::TriggerConst::eT4);
                for (int j=0;j<nMaxWFAsignals;j++)
                {
                    WFA_TimeStructure[1][j] = undefinedValue;
                }
                for (int j=0;j<int(fTriggerRaw.GetTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eT4).size());j++)
                {
                    WFA_TimeStructure[1][j] = fTriggerRaw.GetTimeStructure(det::TimeStructureConst::eWFA,det::TriggerConst::eT4).at(j);
                }
            }
        }
   
        
        NA61Tree -> Fill();
    }
    
    void ConverterShineToNA61Tree::Init_NA61Tree()
    {
        
        //Event

        NA61Tree -> Branch("EventInfo_Event_Id", &EventInfo_Event_Id, "EventInfo_Event_Id/I");
        NA61Tree -> Branch("EventInfo_Run_Id", &EventInfo_Run_Id, "EventInfo_Run_Id/I");
        NA61Tree -> Branch("EventInfo_Event_Timestamp", &EventInfo_Event_Timestamp, "EventInfo_Event_Timestamp/I");
        
        //Track Parameters, covariant matrix
        NA61Tree -> Branch("trackParameters_qOverPXZ",trackParameters_qOverPXZ,Form("trackParameters_qOverPXZ[%i]/F",nMaxTracks));
        NA61Tree -> Branch("trackParameters_tanLambda",trackParameters_tanLambda,Form("trackParameters_tanLambda[%i]/F",nMaxTracks));
        NA61Tree -> Branch("trackParameters_phi",trackParameters_phi,Form("trackParameters_phi[%i]/F",nMaxTracks));
        NA61Tree -> Branch("trackParameters_vtx_PositionX",trackParameters_vtx_PositionX,Form("trackParameters_vtx_PositionX[%i]/F",nMaxTracks));
        NA61Tree -> Branch("trackParameters_vtx_PositionY",trackParameters_vtx_PositionY,Form("trackParameters_vtx_PositionY[%i]/F",nMaxTracks));
        NA61Tree -> Branch("CovarianceMatrixArray", CovarianceMatrixArray, Form("CovarianceMatrixArray[%i][%i][%i]/F",nMaxTracks,nTrackParameters,nTrackParameters));
        
        
        //Main vertex = Data Base vertex (DBVtx). It should be renamed properly.
        NA61Tree -> Branch("Main_Vertex_X", &Main_Vertex_X, "Main_Vertex_X/F");
        NA61Tree -> Branch("Main_Vertex_Y", &Main_Vertex_Y, "Main_Vertex_Y/F");
        NA61Tree -> Branch("Main_Vertex_Z", &Main_Vertex_Z, "Main_Vertex_Z/F");
        
        //Primary vertex = Reconstructed vertex (RecoVtx). It should be renamed properly.
        NA61Tree -> Branch("Primary_Vertex_X", &Primary_Vertex_X, "Primary_Vertex_X/F");
        NA61Tree -> Branch("Primary_Vertex_Y", &Primary_Vertex_Y, "Primary_Vertex_Y/F");
        NA61Tree -> Branch("Primary_Vertex_Z", &Primary_Vertex_Z, "Primary_Vertex_Z/F");
        NA61Tree -> Branch("Has_Primary_Vertex", &Has_Primary_Vertex, "Has_Primary_Vertex/O");
        NA61Tree -> Branch("Primary_Vertex_Q", &Primary_Vertex_Q, "Primary_Vertex_Q/I");
        
        //TPC
        NA61Tree -> Branch("Track_DBVtx_track_pT", Track_DBVtx_track_pT, Form("Track_DBVtx_track_pT[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_px", Track_DBVtx_track_px, Form("Track_DBVtx_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_py", Track_DBVtx_track_py, Form("Track_DBVtx_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_pz", Track_DBVtx_track_pz, Form("Track_DBVtx_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_eta", Track_DBVtx_track_eta, Form("Track_DBVtx_track_eta[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_phi", Track_DBVtx_track_phi, Form("Track_DBVtx_track_phi[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_point_x", Track_DBVtx_track_point_x, Form("Track_DBVtx_track_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_point_y", Track_DBVtx_track_point_y, Form("Track_DBVtx_track_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_point_z", Track_DBVtx_track_point_z, Form("Track_DBVtx_track_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_first_point_x", Track_DBVtx_track_first_point_x, Form("Track_DBVtx_track_first_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_first_point_y", Track_DBVtx_track_first_point_y, Form("Track_DBVtx_track_first_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_first_point_z", Track_DBVtx_track_first_point_z, Form("Track_DBVtx_track_first_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_last_point_x", Track_DBVtx_track_last_point_x, Form("Track_DBVtx_track_last_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_last_point_y", Track_DBVtx_track_last_point_y, Form("Track_DBVtx_track_last_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_last_point_z", Track_DBVtx_track_last_point_z, Form("Track_DBVtx_track_last_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_pT", Track_DBVtx_pT, Form("Track_DBVtx_pT[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_px", Track_DBVtx_px, Form("Track_DBVtx_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_py", Track_DBVtx_py, Form("Track_DBVtx_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_pz", Track_DBVtx_pz, Form("Track_DBVtx_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_eta", Track_DBVtx_eta, Form("Track_DBVtx_eta[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_phi", Track_DBVtx_phi, Form("Track_DBVtx_phi[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_track_px", Track_extDBVtx_track_px, Form("Track_extDBVtx_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_track_py", Track_extDBVtx_track_py, Form("Track_extDBVtx_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_track_pz", Track_extDBVtx_track_pz, Form("Track_extDBVtx_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_track_px", Track_extDBVtx_first_track_px, Form("Track_extDBVtx_first_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_track_py", Track_extDBVtx_first_track_py, Form("Track_extDBVtx_first_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_track_pz", Track_extDBVtx_first_track_pz, Form("Track_extDBVtx_first_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_track_px", Track_extDBVtx_last_track_px, Form("Track_extDBVtx_last_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_track_py", Track_extDBVtx_last_track_py, Form("Track_extDBVtx_last_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_track_pz", Track_extDBVtx_last_track_pz, Form("Track_extDBVtx_last_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_point_x", Track_extDBVtx_point_x, Form("Track_extDBVtx_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_point_y", Track_extDBVtx_point_y, Form("Track_extDBVtx_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_point_z", Track_extDBVtx_point_z, Form("Track_extDBVtx_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_point_x", Track_extDBVtx_first_point_x, Form("Track_extDBVtx_first_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_point_y", Track_extDBVtx_first_point_y, Form("Track_extDBVtx_first_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_first_point_z", Track_extDBVtx_first_point_z, Form("Track_extDBVtx_first_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_point_x", Track_extDBVtx_last_point_x, Form("Track_extDBVtx_last_point_x[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_point_y", Track_extDBVtx_last_point_y, Form("Track_extDBVtx_last_point_y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDBVtx_last_point_z", Track_extDBVtx_last_point_z, Form("Track_extDBVtx_last_point_z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_track_px", 		Track_extDB_test_track_px,			Form("Track_extDB_test_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_track_py", 		Track_extDB_test_track_py,			Form("Track_extDB_test_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_track_pz", 		Track_extDB_test_track_pz,			Form("Track_extDB_test_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_track_px", Track_extDB_test_first_track_px, 	Form("Track_extDB_test_first_track_px[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_track_py", Track_extDB_test_first_track_py, 	Form("Track_extDB_test_first_track_py[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_track_pz", Track_extDB_test_first_track_pz, 	Form("Track_extDB_test_first_track_pz[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_track_px", 	Track_extDB_test_last_track_px, 	Form("Track_extDB_test_last_track_px[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_track_py", 	Track_extDB_test_last_track_py, 	Form("Track_extDB_test_last_track_py[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_track_pz", 	Track_extDB_test_last_track_pz, 	Form("Track_extDB_test_last_track_pz[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_point_x", 		Track_extDB_test_point_x, 			Form("Track_extDB_test_point_x[%i]/F",			nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_point_y", 		Track_extDB_test_point_y, 			Form("Track_extDB_test_point_y[%i]/F",			nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_point_z", 		Track_extDB_test_point_z, 			Form("Track_extDB_test_point_z[%i]/F",			nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_point_x", 	Track_extDB_test_first_point_x, 	Form("Track_extDB_test_first_point_x[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_point_y", 	Track_extDB_test_first_point_y, 	Form("Track_extDB_test_first_point_y[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_first_point_z", 	Track_extDB_test_first_point_z, 	Form("Track_extDB_test_first_point_z[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_point_x", 	Track_extDB_test_last_point_x, 	Form("Track_extDB_test_last_point_x[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_point_y", 	Track_extDB_test_last_point_y, 	Form("Track_extDB_test_last_point_y[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_test_last_point_z", 	Track_extDB_test_last_point_z, 	Form("Track_extDB_test_last_point_z[%i]/F",	nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_track_px", 		Track_extDB_PSD_track_px,			Form("Track_extDB_PSD_track_px[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_track_py", 		Track_extDB_PSD_track_py,			Form("Track_extDB_PSD_track_py[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_track_pz", 		Track_extDB_PSD_track_pz,			Form("Track_extDB_PSD_track_pz[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_point_x", 		Track_extDB_PSD_point_x, 			Form("Track_extDB_PSD_point_x[%i]/F",			nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_point_y", 		Track_extDB_PSD_point_y, 			Form("Track_extDB_PSD_point_y[%i]/F",			nMaxTracks));
        NA61Tree -> Branch("Track_extDB_PSD_point_z", 		Track_extDB_PSD_point_z, 			Form("Track_extDB_PSD_point_z[%i]/F",			nMaxTracks));
        
        NA61Tree -> Branch("Track_DBVtx_impact_point_X", Track_DBVtx_impact_point_X, Form("Track_DBVtx_impact_point_X[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_impact_point_Y", Track_DBVtx_impact_point_Y, Form("Track_DBVtx_impact_point_Y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_impact_point_Z", Track_DBVtx_impact_point_Z, Form("Track_DBVtx_impact_point_Z[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_pT", Track_RecoVtx_track_pT, Form("Track_RecoVtx_track_pT[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_eta", Track_RecoVtx_track_eta, Form("Track_RecoVtx_track_eta[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_phi", Track_RecoVtx_track_phi, Form("Track_RecoVtx_track_phi[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_pT", Track_RecoVtx_pT, Form("Track_RecoVtx_pT[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_eta", Track_RecoVtx_eta, Form("Track_RecoVtx_eta[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_phi", Track_RecoVtx_phi, Form("Track_RecoVtx_phi[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_impact_point_X", Track_RecoVtx_impact_point_X, Form("Track_RecoVtx_impact_point_X[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_impact_point_Y", Track_RecoVtx_impact_point_Y, Form("Track_RecoVtx_impact_point_Y[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_impact_point_Z", Track_RecoVtx_impact_point_Z, Form("Track_RecoVtx_impact_point_Z[%i]/F",nMaxTracks));
        
        NA61Tree -> Branch("Track_DBVtx_track_charge", Track_DBVtx_track_charge, Form("Track_DBVtx_track_charge[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClusters_Total", Track_DBVtx_track_nClusters_Total, Form("Track_DBVtx_track_nClusters_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClusters_TPCV1", Track_DBVtx_track_nClusters_TPCV1, Form("Track_DBVtx_track_nClusters_TPCV1[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClusters_TPCV2", Track_DBVtx_track_nClusters_TPCV2, Form("Track_DBVtx_track_nClusters_TPCV2[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClusters_TPCVmain", Track_DBVtx_track_nClusters_TPCVmain, Form("Track_DBVtx_track_nClusters_TPCVmain[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClusters_TPCVgap", Track_DBVtx_track_nClusters_TPCVgap, Form("Track_DBVtx_track_nClusters_TPCVgap[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersPotential_Total", Track_DBVtx_track_nClustersPotential_Total, Form("Track_DBVtx_track_nClustersPotential_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersdEdX_Total", Track_DBVtx_track_nClustersdEdX_Total, Form("Track_DBVtx_track_nClustersdEdX_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersdEdX_TPCV1", Track_DBVtx_track_nClustersdEdX_TPCV1, Form("Track_DBVtx_track_nClustersdEdX_TPCV1[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersdEdX_TPCV2", Track_DBVtx_track_nClustersdEdX_TPCV2, Form("Track_DBVtx_track_nClustersdEdX_TPCV2[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersdEdX_TPCVmain", Track_DBVtx_track_nClustersdEdX_TPCVmain, Form("Track_DBVtx_track_nClustersdEdX_TPCVmain[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_nClustersdEdX_TPCVgap", Track_DBVtx_track_nClustersdEdX_TPCVgap, Form("Track_DBVtx_track_nClustersdEdX_TPCVgap[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_EnergyClusters_Total", Track_DBVtx_track_EnergyClusters_Total, Form("Track_DBVtx_track_EnergyClusters_Total[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_EnergyClusters_TPCV1", Track_DBVtx_track_EnergyClusters_TPCV1, Form("Track_DBVtx_track_EnergyClusters_TPCV1[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_EnergyClusters_TPCV2", Track_DBVtx_track_EnergyClusters_TPCV2, Form("Track_DBVtx_track_EnergyClusters_TPCV2[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_EnergyClusters_TPCVmain", Track_DBVtx_track_EnergyClusters_TPCVmain, Form("Track_DBVtx_track_EnergyClusters_TPCVmain[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_EnergyClusters_TPCVgap", Track_DBVtx_track_EnergyClusters_TPCVgap, Form("Track_DBVtx_track_EnergyClusters_TPCVgap[%i]/D",nMaxTracks));      
        NA61Tree -> Branch("Track_DBVtx_track_chi2", Track_DBVtx_track_chi2, Form("Track_DBVtx_track_chi2[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_DBVtx_track_ndf", Track_DBVtx_track_ndf, Form("Track_DBVtx_track_ndf[%i]/I",nMaxTracks));
        
        NA61Tree -> Branch("Track_RecoVtx_track_charge", Track_RecoVtx_track_charge, Form("Track_RecoVtx_track_charge[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClusters_Total", Track_RecoVtx_track_nClusters_Total, Form("Track_RecoVtx_track_nClusters_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClusters_TPCV1", Track_RecoVtx_track_nClusters_TPCV1, Form("Track_RecoVtx_track_nClusters_TPCV1[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClusters_TPCV2", Track_RecoVtx_track_nClusters_TPCV2, Form("Track_RecoVtx_track_nClusters_TPCV2[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClusters_TPCVmain", Track_RecoVtx_track_nClusters_TPCVmain, Form("Track_RecoVtx_track_nClusters_TPCVmain[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClusters_TPCVgap", Track_RecoVtx_track_nClusters_TPCVgap, Form("Track_RecoVtx_track_nClusters_TPCVgap[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersPotential_Total", Track_RecoVtx_track_nClustersPotential_Total, Form("Track_RecoVtx_track_nClustersPotential_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersdEdX_Total", Track_RecoVtx_track_nClustersdEdX_Total, Form("Track_RecoVtx_track_nClustersdEdX_Total[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersdEdX_TPCV1", Track_RecoVtx_track_nClustersdEdX_TPCV1, Form("Track_RecoVtx_track_nClustersdEdX_TPCV1[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersdEdX_TPCV2", Track_RecoVtx_track_nClustersdEdX_TPCV2, Form("Track_RecoVtx_track_nClustersdEdX_TPCV2[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersdEdX_TPCVmain", Track_RecoVtx_track_nClustersdEdX_TPCVmain, Form("Track_RecoVtx_track_nClustersdEdX_TPCVmain[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_nClustersdEdX_TPCVgap", Track_RecoVtx_track_nClustersdEdX_TPCVgap, Form("Track_RecoVtx_track_nClustersdEdX_TPCVgap[%i]/I",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_EnergyClusters_Total", Track_RecoVtx_track_EnergyClusters_Total, Form("Track_RecoVtx_track_EnergyClusters_Total[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_EnergyClusters_TPCV1", Track_RecoVtx_track_EnergyClusters_TPCV1, Form("Track_RecoVtx_track_EnergyClusters_TPCV1[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_EnergyClusters_TPCV2", Track_RecoVtx_track_EnergyClusters_TPCV2, Form("Track_RecoVtx_track_EnergyClusters_TPCV2[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_EnergyClusters_TPCVmain", Track_RecoVtx_track_EnergyClusters_TPCVmain, Form("Track_RecoVtx_track_EnergyClusters_TPCVmain[%i]/D",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_EnergyClusters_TPCVgap", Track_RecoVtx_track_EnergyClusters_TPCVgap, Form("Track_RecoVtx_track_EnergyClusters_TPCVgap[%i]/D",nMaxTracks));      
        NA61Tree -> Branch("Track_RecoVtx_track_chi2", Track_RecoVtx_track_chi2, Form("Track_RecoVtx_track_chi2[%i]/F",nMaxTracks));
        NA61Tree -> Branch("Track_RecoVtx_track_ndf", Track_RecoVtx_track_ndf, Form("Track_RecoVtx_track_ndf[%i]/I",nMaxTracks));
        
        
        NA61Tree -> Branch("Track_DBVtx_nTracks", &Track_DBVtx_nTracks, "Track_DBVtx_nTracks/I");
        NA61Tree -> Branch("Track_RecoVtx_nTracks", &Track_RecoVtx_nTracks, "Track_RecoVtx_nTracks/I");

        
        //PSD
        NA61Tree -> Branch("PSD_module_Number", &PSD_module_Number, "PSD_module_Number/I");
        NA61Tree -> Branch("PSD_section_Number", &PSD_section_Number,"PSD_section_Number/I");
        NA61Tree -> Branch("PSD_module_X", PSD_module_X, Form("PSD_module_X[%i]/F",nPSD_Modules));
        NA61Tree -> Branch("PSD_module_Y", PSD_module_Y, Form("PSD_module_Y[%i]/F",nPSD_Modules));
        NA61Tree -> Branch("PSD_module_Z", PSD_module_Z, Form("PSD_module_Z[%i]/F",nPSD_Modules));
        NA61Tree -> Branch("PSD_module_Energy", PSD_module_Energy, Form("PSD_module_Energy[%i]/F",nPSD_Modules));
        NA61Tree -> Branch("PSD_module_number_of_sections", PSD_module_number_of_sections, Form("PSD_module_number_of_sections[%i]/I",nPSD_Modules));
        NA61Tree -> Branch("PSD_section_Energy", PSD_section_Energy, Form("PSD_section_Energy[%i][%i]/F",nPSD_Modules,nPSD_Sections));
        NA61Tree -> Branch("PSD_section_Number_array", PSD_section_Number_array, Form("PSD_section_Number_array[%i][%i]/I",nPSD_Modules,nPSD_Sections));
        NA61Tree -> Branch("PSD_Energy", &PSD_Energy, "PSD_Energy/F");
        NA61Tree -> Branch("PSD_1_Energy", &PSD_1_Energy, "PSD_1_Energy/F");
        NA61Tree -> Branch("PSD_2_Energy", &PSD_2_Energy, "PSD_2_Energy/F");
        NA61Tree -> Branch("PSD_3_Energy", &PSD_3_Energy, "PSD_3_Energy/F");

        //Triggers
        NA61Tree -> Branch("BPD_Status", BPD_Status, Form("BPD_Status[%i][%i][%i]/I",nRawReco,nBPD,nBPDcomponents));  
        NA61Tree -> Branch("BPD_Position", BPD_Position, Form("BPD_Position[%i][%i][%i]/F",nRawReco,nBPD,nBPDcomponents)); 
        NA61Tree -> Branch("BPD_PositionError", BPD_PositionError, Form("BPD_PositionError[%i][%i][%i]/F",nRawReco,nBPD,nBPDcomponents)); 
        NA61Tree -> Branch("triggersADC", triggersADC, Form("triggersADC[%i][%i]/F",nRawReco,nTriggers_Simple)); 
        NA61Tree -> Branch("isTriggers_Simple", isTriggers_Simple, Form("isTriggers_Simple[%i][%i]/O",nRawReco,nTriggers_Simple)); 
        NA61Tree -> Branch("isTriggers_Combined", isTriggers_Combined, Form("isTriggers_Combined[%i][%i]/O",nRawReco,nTriggers_Combined)); 
        NA61Tree -> Branch("WFA_TimeStructure", WFA_TimeStructure, Form("WFA_TimeStructure[%i][%i]/F",nTriggersWFA,nMaxWFAsignals)); 
        NA61Tree -> Branch("WFA_NumberOfSignalHits", WFA_NumberOfSignalHits, Form("WFA_NumberOfSignalHits[%i]/I",nTriggersWFA)); 

    }
    
    void ConverterShineToNA61Tree::ClearEvent()
    {
        //       NA61Tree->ResetBranchAddresses();
        EventInfo_Event_Timestamp = 0;
        EventInfo_Event_Id = 0;
        EventInfo_Run_Id = 0;
        
        for (int i=0;i<nMaxTracks;i++)
        {

            for(int j=0; j<nTrackParameters; j++){
            for(int k=0; k<nTrackParameters; k++) {
                    CovarianceMatrixArray[i][k][j]=undefinedValue;
                }
            }
            
            trackParameters_qOverPXZ[i]      = undefinedValue;
            trackParameters_tanLambda[i]     = undefinedValue;
            trackParameters_phi[i]           = undefinedValue;
            trackParameters_vtx_PositionX[i] = undefinedValue;
            trackParameters_vtx_PositionY[i] = undefinedValue;

            
            Sim_track_pT[i] = undefinedValue;
            Sim_track_phi[i] = undefinedValue;
            Sim_track_eta[i] = undefinedValue;
            Sim_track_mass[i] = undefinedValue;
            Sim_track_charge[i] = undefinedValue;
            Sim_track_pdg_id[i] = undefinedValue;
            Sim_track_IsInTPC_Total[i] = undefinedValue;
            Sim_track_IsInTPC_TPCV1[i] = undefinedValue;
            Sim_track_IsInTPC_TPCV2[i] = undefinedValue;
            Sim_track_IsInTPC_TPCVmain[i] = undefinedValue;
            Sim_track_Number_of_Hits[i] = undefinedValue;
            Sim_track_IsSpectator[i] = undefinedValue;
            
            Track_DBVtx_track_pT[i] = undefinedValue;
            Track_DBVtx_track_px[i] = undefinedValue;
            Track_DBVtx_track_py[i] = undefinedValue;
            Track_DBVtx_track_pz[i] = undefinedValue;
            Track_DBVtx_track_eta[i] = undefinedValue;
            Track_DBVtx_track_phi[i] = undefinedValue;
            Track_DBVtx_track_point_x[i] = undefinedValue;
            Track_DBVtx_track_point_y[i] = undefinedValue;
            Track_DBVtx_track_point_z[i] = undefinedValue;
            Track_DBVtx_track_first_point_x[i] = undefinedValue;
            Track_DBVtx_track_first_point_y[i] = undefinedValue;
            Track_DBVtx_track_first_point_z[i] = undefinedValue;
            Track_DBVtx_track_last_point_x[i] = undefinedValue;
            Track_DBVtx_track_last_point_y[i] = undefinedValue;
            Track_DBVtx_track_last_point_z[i] = undefinedValue;
            
            Track_DBVtx_pT[i] = undefinedValue;
            Track_DBVtx_px[i] = undefinedValue;
            Track_DBVtx_py[i] = undefinedValue;
            Track_DBVtx_pz[i] = undefinedValue;
            Track_DBVtx_eta[i] = undefinedValue;
            Track_DBVtx_phi[i] = undefinedValue;
            Track_DBVtx_impact_point_X[i] = undefinedValue;
            Track_DBVtx_impact_point_Y[i] = undefinedValue;
            Track_DBVtx_impact_point_Z[i] = undefinedValue;
            
            Track_extDBVtx_track_px[i] = undefinedValue;
            Track_extDBVtx_track_py[i] = undefinedValue;
            Track_extDBVtx_track_pz[i] = undefinedValue;
            Track_extDBVtx_first_track_px[i] = undefinedValue;
            Track_extDBVtx_first_track_py[i] = undefinedValue;
            Track_extDBVtx_first_track_pz[i] = undefinedValue;
            Track_extDBVtx_last_track_px[i] = undefinedValue;
            Track_extDBVtx_last_track_py[i] = undefinedValue;
            Track_extDBVtx_last_track_pz[i] = undefinedValue;
            Track_extDBVtx_point_x[i] = undefinedValue;
            Track_extDBVtx_point_y[i] = undefinedValue;
            Track_extDBVtx_point_z[i] = undefinedValue;
            Track_extDBVtx_first_point_x[i] = undefinedValue;
            Track_extDBVtx_first_point_y[i] = undefinedValue;
            Track_extDBVtx_first_point_z[i] = undefinedValue;
            Track_extDBVtx_last_point_x[i] = undefinedValue;
            Track_extDBVtx_last_point_y[i] = undefinedValue;
            Track_extDBVtx_last_point_z[i] = undefinedValue;
            
            Track_extDB_test_track_px[i] = undefinedValue;
            Track_extDB_test_track_py[i] = undefinedValue;
            Track_extDB_test_track_pz[i] = undefinedValue;
            Track_extDB_test_first_track_px[i] = undefinedValue;
            Track_extDB_test_first_track_py[i] = undefinedValue;
            Track_extDB_test_first_track_pz[i] = undefinedValue;
            Track_extDB_test_last_track_px[i] = undefinedValue;
            Track_extDB_test_last_track_py[i] = undefinedValue;
            Track_extDB_test_last_track_pz[i] = undefinedValue;
            Track_extDB_test_point_x[i] = undefinedValue;
            Track_extDB_test_point_y[i] = undefinedValue;
            Track_extDB_test_point_z[i] = undefinedValue;
            Track_extDB_test_first_point_x[i] = undefinedValue;
            Track_extDB_test_first_point_y[i] = undefinedValue;
            Track_extDB_test_first_point_z[i] = undefinedValue;
            Track_extDB_test_last_point_x[i] = undefinedValue;
            Track_extDB_test_last_point_y[i] = undefinedValue;
            Track_extDB_test_last_point_z[i] = undefinedValue;
            
            Track_extDB_PSD_track_px[i] = undefinedValue;
            Track_extDB_PSD_track_py[i] = undefinedValue;
            Track_extDB_PSD_track_pz[i] = undefinedValue;
            Track_extDB_PSD_point_x[i] = undefinedValue;
            Track_extDB_PSD_point_y[i] = undefinedValue;
            Track_extDB_PSD_point_z[i] = undefinedValue;
            
            
            Track_RecoVtx_track_pT[i] = undefinedValue;
            Track_RecoVtx_track_eta[i] = undefinedValue;
            Track_RecoVtx_track_phi[i] = undefinedValue;
            
            Track_RecoVtx_pT[i] = undefinedValue;
            Track_RecoVtx_eta[i] = undefinedValue;
            Track_RecoVtx_phi[i] = undefinedValue;
            Track_RecoVtx_impact_point_X[i] = undefinedValue;
            Track_RecoVtx_impact_point_Y[i] = undefinedValue;
            Track_RecoVtx_impact_point_Z[i] = undefinedValue;
            
            Track_DBVtx_track_charge[i] = undefinedValue;
            Track_DBVtx_track_nClusters_Total[i] = undefinedValue;
            Track_DBVtx_track_nClusters_TPCV1[i] = undefinedValue;
            Track_DBVtx_track_nClusters_TPCV2[i] = undefinedValue;
            Track_DBVtx_track_nClusters_TPCVmain[i] = undefinedValue;
            Track_DBVtx_track_nClusters_TPCVgap[i] = undefinedValue;
            Track_DBVtx_track_nClustersPotential_Total[i] = undefinedValue;
            Track_DBVtx_track_nClustersPotential_TPCV1[i] = undefinedValue;
            Track_DBVtx_track_nClustersPotential_TPCV2[i] = undefinedValue;
            Track_DBVtx_track_nClustersPotential_TPCVmain[i] = undefinedValue;
            Track_DBVtx_track_nClustersPotential_TPCVgap[i] = undefinedValue;
            Track_DBVtx_track_nClustersFit_Total[i] = undefinedValue;
            Track_DBVtx_track_nClustersFit_TPCV1[i] = undefinedValue;
            Track_DBVtx_track_nClustersFit_TPCV2[i] = undefinedValue;
            Track_DBVtx_track_nClustersFit_TPCVmain[i] = undefinedValue;
            Track_DBVtx_track_nClustersFit_TPCVgap[i] = undefinedValue;
            Track_DBVtx_track_nClustersdEdX_Total[i] = undefinedValue;
            Track_DBVtx_track_nClustersdEdX_TPCV1[i] = undefinedValue;
            Track_DBVtx_track_nClustersdEdX_TPCV2[i] = undefinedValue;
            Track_DBVtx_track_nClustersdEdX_TPCVmain[i] = undefinedValue;
            Track_DBVtx_track_nClustersdEdX_TPCVgap[i] = undefinedValue;
            Track_DBVtx_track_EnergyClusters_Total[i] = undefinedValue;
            Track_DBVtx_track_EnergyClusters_TPCV1[i] = undefinedValue;
            Track_DBVtx_track_EnergyClusters_TPCV2[i] = undefinedValue;
            Track_DBVtx_track_EnergyClusters_TPCVmain[i] = undefinedValue;
            Track_DBVtx_track_EnergyClusters_TPCVgap[i] = undefinedValue;
            Track_DBVtx_track_DCAtoVertex_X[i] = undefinedValue;
            Track_DBVtx_track_DCAtoVertex_Y[i] = undefinedValue;
            Track_DBVtx_track_DCAtoVertex_Z[i] = undefinedValue;
            Track_DBVtx_track_chi2[i] = undefinedValue;  
            Track_DBVtx_track_ndf[i] = undefinedValue;  
            
            Track_RecoVtx_track_charge[i] = undefinedValue;
            Track_RecoVtx_track_nClusters_Total[i] = undefinedValue;
            Track_RecoVtx_track_nClusters_TPCV1[i] = undefinedValue;
            Track_RecoVtx_track_nClusters_TPCV2[i] = undefinedValue;
            Track_RecoVtx_track_nClusters_TPCVmain[i] = undefinedValue;
            Track_RecoVtx_track_nClusters_TPCVgap[i] = undefinedValue;
            Track_RecoVtx_track_nClustersPotential_Total[i] = undefinedValue;
            Track_RecoVtx_track_nClustersPotential_TPCV1[i] = undefinedValue;
            Track_RecoVtx_track_nClustersPotential_TPCV2[i] = undefinedValue;
            Track_RecoVtx_track_nClustersPotential_TPCVmain[i] = undefinedValue;
            Track_RecoVtx_track_nClustersPotential_TPCVgap[i] = undefinedValue;
            Track_RecoVtx_track_nClustersFit_Total[i] = undefinedValue;
            Track_RecoVtx_track_nClustersFit_TPCV1[i] = undefinedValue;
            Track_RecoVtx_track_nClustersFit_TPCV2[i] = undefinedValue;
            Track_RecoVtx_track_nClustersFit_TPCVmain[i] = undefinedValue;
            Track_RecoVtx_track_nClustersFit_TPCVgap[i] = undefinedValue;
            Track_RecoVtx_track_nClustersdEdX_Total[i] = undefinedValue;
            Track_RecoVtx_track_nClustersdEdX_TPCV1[i] = undefinedValue;
            Track_RecoVtx_track_nClustersdEdX_TPCV2[i] = undefinedValue;
            Track_RecoVtx_track_nClustersdEdX_TPCVmain[i] = undefinedValue;
            Track_RecoVtx_track_nClustersdEdX_TPCVgap[i] = undefinedValue;
            Track_RecoVtx_track_EnergyClusters_Total[i] = undefinedValue;
            Track_RecoVtx_track_EnergyClusters_TPCV1[i] = undefinedValue;
            Track_RecoVtx_track_EnergyClusters_TPCV2[i] = undefinedValue;
            Track_RecoVtx_track_EnergyClusters_TPCVmain[i] = undefinedValue;
            Track_RecoVtx_track_EnergyClusters_TPCVgap[i] = undefinedValue;
            Track_RecoVtx_track_DCAtoVertex_X[i] = undefinedValue;
            Track_RecoVtx_track_DCAtoVertex_Y[i] = undefinedValue;
            Track_RecoVtx_track_DCAtoVertex_Z[i] = undefinedValue;
            Track_RecoVtx_track_chi2[i] = undefinedValue;  
            Track_RecoVtx_track_ndf[i] = undefinedValue; 
        }
        Sim_nTracks = 0;
        Sim_PSD_Energy = 0;
        Track_DBVtx_nTracks = 0;
        Track_RecoVtx_nTracks = 0;
        TPC_Multiplicity = 0;
        TPC_Multiplicity_all = 0;
        Primary_Multiplicity_all = 0;
        TPC_Multiplicity_Clusters_All = 0;
        TPC_Multiplicity_Clusters_VTPC1_VTPC2 = 0;
        TPC_cos1=0;
        TPC_sin1=0;
        TPC_cos2=0;
        TPC_sin2=0;
        
        PSD_Energy = 0;
        PSD_1_Energy = 0;
        PSD_2_Energy = 0;
        PSD_3_Energy = 0;
        PSD_section_Number = 0;
        PSD_module_Number = 0;
        for (int i=0;i<nPSD_Modules;i++)
        {
            for (int j=0;j<nPSD_Sections;j++)
            {
                PSD_section_Energy[i][j] = 0;
                PSD_section_Number_array[i][j] = 0;
                Sim_PSD_section_Energy[i][j] = 0;
            }
            PSD_module_X[i] = 0;
            PSD_module_Y[i] = 0;
            PSD_module_Z[i] = 17000;
            PSD_module_Energy[i] = 0;
            PSD_module_number_of_sections[i] = 0;
            Sim_PSD_module_Energy[i] = 0;
            Sim_PSD_module_Temperature[i] = 0;
        }
        PSD_module_Z[44] = 17000.0-259.8;
        for (int i=0;i<nPSD_Sections;i++)
        {
            PSD_section_slice_Energy[i] = 0;
        }
        
        for (int k=0;k<nRawReco;k++)
        {
            for (int i=0;i<nBPD;i++)
            {
                for (int j=0;j<nBPDcomponents;j++)
                {
                    BPD_Status[k][i][j] = undefinedValue;
                    BPD_Position[k][i][j] = undefinedValue;
                    BPD_PositionError[k][i][j] = undefinedValue;
                    BPD_Z[k][i][j] = undefinedValue;
                    BPD_RMS[k][i][j] = undefinedValue;
                    BPD_Maximum[k][i][j] = undefinedValue;
                    BPD_Charge[k][i][j] = undefinedValue;
                    BPD_SumOfAll[k][i][j] = undefinedValue;
                }
            }
            for (int i=0;i<nTriggers_Simple;i++)
            {
                triggersADC[k][i] = undefinedValue;
                isTriggers_Simple[k][i] = false;
            }
            for (int i=0;i<nTriggers_Combined;i++)
            {
                isTriggers_Combined[k][i] = false;
            }
            for (int i=0;i<nBeamComponents;i++)
            {
                Beam_Momentum[k][i] = undefinedValue;
                Beam_Fitted2DLineXZ[k][i] = undefinedValue;
                Beam_Fitted2DLineYZ[k][i] = undefinedValue;
            }
            Beam_Status[k] = undefinedValue;
        }
        for (int i=0;i<nTriggersWFA;i++)
        {
            for (int j=0;j<nMaxWFAsignals;j++)
            {
                WFA_TimeStructure[i][j] = undefinedValue;
            }
            WFA_NumberOfSignalHits[i] = undefinedValue;
        }
        

    }
    
    
}
