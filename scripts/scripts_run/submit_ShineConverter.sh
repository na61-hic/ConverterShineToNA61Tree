#!/bin/bash
timestamp=$(date +%Y%m%d_%H%M%S)
run_ShineConverter_dir=`pwd`

echo "submit_ShineConverter.sh: ========================= STARTED at $timestamp ========================="

shine_converter_source_dir=$1 #directory where all the scripts and filelists directory are stored
shine_dir=$2 #path to shine directory, for example "/afs/cern.ch/user/a/agrobov/public/shine/" 
filelist_name=$3 #name of file with list of files, one job will be submitted for each file in list
top_output_dir=$4
run_on_batch=$5
label=$6 #label to distinguish between different runs (for your convenience)
nEvents=$7 # all event: "unbounded", otherwise specify a number


input_filelist=$shine_converter_source_dir/"filelists"/$filelist_name".list"

echo "submit_ShineConverter.sh: shine_converter_source_dir "$shine_converter_source_dir
echo "submit_ShineConverter.sh: shine_dir                  "$shine_dir
echo "submit_ShineConverter.sh: filelist_name              "$filelist_name
echo "submit_ShineConverter.sh: top_output_dir             "$top_output_dir
echo "submit_ShineConverter.sh: run_on_batch               "$run_on_batch
echo "submit_ShineConverter.sh: label                      "$label
echo "submit_ShineConverter.sh: nEvents                    "$nEvents

# queue="8nh" #options: "8nh" "1nd"
queue="1nh" #options: "8nh" "1nd"

source_scripts_shine=$shine_converter_source_dir/"scripts_shine"
source_scripts_run=$shine_converter_source_dir/"scripts_run"


output_dir=$top_output_dir/$timestamp"_"$label
log_dir=$output_dir/"log"
scripts_shine=$output_dir/"scripts_shine"
scripts_run=$output_dir/"scripts_run"
filelists_dir=$output_dir/"filelists"
NA61Tree_dir=$output_dir/"NA61Tree"
# filelist_conf=$filelist".conf"
script_run=$scripts_run/"run_ShineConverter.sh"
runModuleSeq_config=$filelists_dir/$filelist_name"_runModuleSeq.config"

echo "submit_ShineConverter.sh: output_dir    "$output_dir
echo "submit_ShineConverter.sh: log_dir       "$log_dir
echo "submit_ShineConverter.sh: scripts_shine "$scripts_shine
echo "submit_ShineConverter.sh: scripts_run   "$scripts_run
echo "submit_ShineConverter.sh: filelists_dir "$filelists_dir
echo "submit_ShineConverter.sh: NA61Tree_dir  "$NA61Tree_dir
echo "submit_ShineConverter.sh: filelist      "$input_filelist
# echo "submit_ShineConverter.sh: filelist_conf "$filelist_conf
echo "submit_ShineConverter.sh: script_run    "$script_run

mkdir "$output_dir"
mkdir "$log_dir"
mkdir "$scripts_shine"
mkdir "$scripts_run"
mkdir "$filelists_dir"
mkdir "$NA61Tree_dir"

rsync -a $source_scripts_shine/ $scripts_shine/
rsync -a $source_scripts_run/ $scripts_run/
rsync -a $input_filelist $filelists_dir/
rsync -a $shine_converter_source_dir/"filelists"/$filelist_name"_runModuleSeq.config" $filelists_dir/

sed -i -e 's/@NUMBER_OF_EVENTS@/'"$nEvents"'/g' $scripts_shine/"ModuleSequenceRecoToTreeNA61.xml"


echo "submit_ShineConverter.sh: Change to scripts_runs directory "$scripts_runs
cd $scripts_runs

while read inputfile; do
  short_inputfile=`echo $inputfile | cut -f 2 -d "-" | cut -f 1 -d "."`
  logfile="$log_dir"/"$short_inputfile".log
  is_file_at_castor=0
  top_path_inputfile=`echo "$inputfile" | cut -d "/" -f 2`
  [ $top_path_inputfile = "castor" ] && is_file_at_castor=1
  echo "submit_ShineConverter.sh: inputfile         "$inputfile
  echo "submit_ShineConverter.sh: short_inputfile   "$short_inputfile
  echo "submit_ShineConverter.sh: is_file_at_castor "$is_file_at_castor
  echo "submit_ShineConverter.sh: logfile           "$logfile

  if [ "$run_on_batch" = "RUN_ON_BATCH" ]
    then
        batch_log_file=$log_dir/$short_inputfile"_batch.log"
        echo "TEST RUN_ON_BATCH"
#         bsub -q "$queue" -o "$logfile" run.sh "$shine_dir" "$filelist".conf "$inputfile" "$output_dir" "$castor_dir" "$global_key" "$short_inputfile" "$is_file_at_castor" "$bit" "$run_on_batch"
        bsub -q "$queue" -o $batch_log_file $script_run $shine_dir $inputfile $output_dir $short_inputfile $is_file_at_castor $run_on_batch $logfile $NA61Tree_dir $runModuleSeq_config $scripts_shine
    else
        $script_run $shine_dir $inputfile $output_dir $short_inputfile $is_file_at_castor $run_on_batch $logfile $NA61Tree_dir $runModuleSeq_config $scripts_shine
    fi
done <"$input_filelist"

cd $run_ShineConverter_dir

echo "submit_ShineConverter.sh: ========================= END ========================="

