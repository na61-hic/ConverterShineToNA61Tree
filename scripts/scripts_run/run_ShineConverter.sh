#!/bin/bash

timestamp_start=$(date +%Y%m%d_%H%M%S)

# $shine_dir $inputfile $output_dir $short_inputfile $is_file_at_castor $run_on_batch $logfile

shine_dir=$1
inputfile=$2
output_dir=$3
short_inputfile=$4
is_file_at_castor=$5
run_on_batch=$6
logfile=$7
NA61Tree_dir=$8
runModuleSeq_config=$9
scripts_shine=${10}
NA61Tree_extension=".NA61Tree.root"

echo "run_ShineConverter.sh: shine_dir           "$shine_dir      
echo "run_ShineConverter.sh: inputfile           "$inputfile      
echo "run_ShineConverter.sh: output_dir          "$output_dir       
echo "run_ShineConverter.sh: short_inputfile     "$short_inputfile
echo "run_ShineConverter.sh: is_file_at_castor   "$is_file_at_castor       
echo "run_ShineConverter.sh: run_on_batch        "$run_on_batch   
echo "run_ShineConverter.sh: logfile             "$logfile
echo "run_ShineConverter.sh: runModuleSeq_config "$runModuleSeq_config

Na61TreeFile=$NA61Tree_dir/"run-"$short_inputfile$NA61Tree_extension

bootstrapfile="bootstrapRecoToTreeNA61.xml"

if [ "$run_on_batch" = "RUN_ON_BATCH" ]
then
    temp_dir=/pool/lsf/`whoami`/"$short_inputfile"
    rm -rf $temp_dir
    mkdir -p $temp_dir
    batch_dir="$output_dir"/log/batch
    cp "$batch_dir"/*xml* "$temp_dir"
    cp "$batch_dir"/*sh "$temp_dir"
    cp "$batch_dir"/Makefile "$temp_dir"
else
    temp_dir=$scripts_shine
fi

echo "run_ShineConverter.sh: temp_dir: "$temp_dir

temp_outputfile=$temp_dir/$short_inputfile$NA61Tree_extension

temp_inputfile=$temp_dir/$short_inputfile".reco.root"

if [ $is_file_at_castor -eq 1 ]
then
  export STAGE_HOST=castorpublic
  export RFIO_USE_CASTOR_V2=YES
  export STAGE_SVCCLASS=amsuser
  export CASTOR_INSTANCE=castorpublic
  export STAGE_SVCCLASS=na61
  stager_get -M $inputfile
  xrdcp "root://castorpublic.cern.ch//"$inputfile $temp_inputfile
else
  temp_inputfile=$inputfile
fi

echo "run_ShineConverter.sh: temp_inputfile: " $temp_inputfile
echo "run_ShineConverter.sh: temp_outputfile: " $temp_outputfile

cd "$temp_dir"
source $shine_dir/setShine
source $runModuleSeq_config # set variables for options of runModuleSeq.sh

echo "run_ShineConverter.sh: global_key "$NA61_TREE_CONVERTER_GLOBAL_KEY
echo "run_ShineConverter.sh: vertex_fit "$NA61_TREE_CONVERTER_VTX_FIT
echo "run_ShineConverter.sh: mag_field  "$NA61_TREE_CONVERTER_MAG_FIELD

echo "run_ShineConverter.sh: runModuleSeq.sh Writing log to file: "$logfile

. runModuleSeq.sh -i "$temp_inputfile" -o "$temp_outputfile" -b "$bootstrapfile" -k "$NA61_TREE_CONVERTER_GLOBAL_KEY" -v "$NA61_TREE_CONVERTER_VTX_FIT" -m "$NA61_TREE_CONVERTER_MAG_FIELD" >& $logfile

mv $temp_outputfile $Na61TreeFile

echo "run_ShineConverter.sh: Na61TreeFile outpufile: "$Na61TreeFile

timestamp_end=$(date +%Y%m%d_%H%M%S)
echo "run_ShineConverter.sh: FINISHED: TIME: ""$timestamp_start"" -- ""$timestamp_end"



