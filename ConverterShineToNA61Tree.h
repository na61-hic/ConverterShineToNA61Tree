/**
 *   \file
 *   Class ConverterShineToNA61Tree
 *   \author V. Blinov, I. Selyuzhenkov, P. Parfenov, A. Grobov, V. Klochkov
 *   \date 01/01/2015
 */

#ifndef _ConverterShineToNA61Tree_ConverterShineToNA61Tree_h_
#define _ConverterShineToNA61Tree_ConverterShineToNA61Tree_h_

#include <fwk/VModule.h>
#include <utl/Branch.h>
#include <map>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <evt/rec/Cluster.h>
#include <TNtuple.h>
#include <TMath.h> 
#include <TTree.h> 
#include "bitset"
#include <vector>

namespace ConverterShineToNA61Tree
{
    
    class ConverterShineToNA61Tree : public fwk::VModule {
        
    public:
        fwk::VModule::EResultFlag Init();
        fwk::VModule::EResultFlag Process(evt::Event& event, const utl::AttributeMap& attr);
        fwk::VModule::EResultFlag Finish();

        
    private:
        
        enum eNA61Tree {
            nMaxTracks = 2000,
            undefinedValue = -999,
            nPSD_Sections = 10,
            nPSD_Modules = 45,
            nBPDcomponents = 3, 		//0=x, 1=y, 2=z,
            nTriggers_Simple = 6, 		// s1, s2, s3, v1, v1p, PSD
            nTriggers_Combined = 4, 		// T1, T2, T3, T4
            nBPD = 3, 			//BPD1, BPD2, BPD3
            nRawReco = 2, 			//0=raw, 1=reco
            nBeamComponents = 3, 		//0=x, 1=y, 2=z
            nMaxWFAsignals = 2000,
            nTriggersWFA = 2,                   //beam, interaction 
            nTrackParameters = 5  // this number compares to utl::TrackParameters::eNTrackParameters in ConverterShineToNA61Tree.cc so they should be equal. If not - there is a mistake.
        };
        
    private:
        void Convert(evt::Event& event);
        void Init_NA61Tree();
        void ClearEvent();
        TTree* NA61Tree;
        
        //init
        std::string fNA61TreeFileName;
        
        //         std::bitset<8> trigBit;
        
        //Event
        int EventInfo_Event_Timestamp;
        int EventInfo_Event_Id;
        int EventInfo_Run_Id;
        
        //SimEvent
        //Event
        float Sim_ImpactParameter;
        float Sim_ReactionPlaneAngle;
        //Tracks
        float Sim_track_pT[nMaxTracks];
        float Sim_track_phi[nMaxTracks];
        float Sim_track_eta[nMaxTracks];
        float Sim_track_mass[nMaxTracks];
        float Sim_track_charge[nMaxTracks];
        int Sim_track_pdg_id[nMaxTracks];
        bool Sim_track_IsInTPC_Total[nMaxTracks];
        bool Sim_track_IsInTPC_TPCV1[nMaxTracks];
        bool Sim_track_IsInTPC_TPCV2[nMaxTracks];
        bool Sim_track_IsInTPC_TPCVmain[nMaxTracks];
        int Sim_track_Number_of_Hits[nMaxTracks];
        bool Sim_track_IsSpectator[nMaxTracks];
        int Sim_nTracks;
        
        //PSD
        float Sim_PSD_section_Energy[nPSD_Modules][nPSD_Sections];
        float Sim_PSD_module_Energy[nPSD_Modules];
        float Sim_PSD_module_Temperature[nPSD_Modules];
        float Sim_PSD_Energy;
        //RecEvent    
        //TPC
        int Track_DBVtx_nTracks;
        int Track_RecoVtx_nTracks;
        
        
        //track Parameters
        
        float CovarianceMatrixArray[nMaxTracks][nTrackParameters][nTrackParameters];
        double trackParameters_qOverPXZ[nMaxTracks];
        double trackParameters_tanLambda[nMaxTracks];
        double trackParameters_phi[nMaxTracks];
        double trackParameters_vtx_PositionX[nMaxTracks];
        double trackParameters_vtx_PositionY[nMaxTracks];
        
        
        //
        
        float Track_DBVtx_track_pT[nMaxTracks];
        float Track_DBVtx_track_px[nMaxTracks];
        float Track_DBVtx_track_py[nMaxTracks];
        float Track_DBVtx_track_pz[nMaxTracks];
        float Track_DBVtx_track_eta[nMaxTracks];
        float Track_DBVtx_track_phi[nMaxTracks];
        float Track_DBVtx_track_point_x[nMaxTracks];
        float Track_DBVtx_track_point_y[nMaxTracks];
        float Track_DBVtx_track_point_z[nMaxTracks];
        float Track_DBVtx_track_first_point_x[nMaxTracks];
        float Track_DBVtx_track_first_point_y[nMaxTracks];
        float Track_DBVtx_track_first_point_z[nMaxTracks];
        float Track_DBVtx_track_last_point_x[nMaxTracks];
        float Track_DBVtx_track_last_point_y[nMaxTracks];
        float Track_DBVtx_track_last_point_z[nMaxTracks];
        float Track_extDBVtx_track_px[nMaxTracks];
        float Track_extDBVtx_track_py[nMaxTracks];
        float Track_extDBVtx_track_pz[nMaxTracks];
        float Track_extDBVtx_first_track_px[nMaxTracks];
        float Track_extDBVtx_first_track_py[nMaxTracks];
        float Track_extDBVtx_first_track_pz[nMaxTracks];
        float Track_extDBVtx_last_track_px[nMaxTracks];
        float Track_extDBVtx_last_track_py[nMaxTracks];
        float Track_extDBVtx_last_track_pz[nMaxTracks];
        float Track_extDBVtx_point_x[nMaxTracks];
        float Track_extDBVtx_point_y[nMaxTracks];
        float Track_extDBVtx_point_z[nMaxTracks];
        float Track_extDBVtx_first_point_x[nMaxTracks];
        float Track_extDBVtx_first_point_y[nMaxTracks];
        float Track_extDBVtx_first_point_z[nMaxTracks];
        float Track_extDBVtx_last_point_x[nMaxTracks];
        float Track_extDBVtx_last_point_y[nMaxTracks];
        float Track_extDBVtx_last_point_z[nMaxTracks];
        
        float Track_extDB_test_track_px[nMaxTracks];
        float Track_extDB_test_track_py[nMaxTracks];
        float Track_extDB_test_track_pz[nMaxTracks];
        float Track_extDB_test_first_track_px[nMaxTracks];
        float Track_extDB_test_first_track_py[nMaxTracks];
        float Track_extDB_test_first_track_pz[nMaxTracks];
        float Track_extDB_test_last_track_px[nMaxTracks];
        float Track_extDB_test_last_track_py[nMaxTracks];
        float Track_extDB_test_last_track_pz[nMaxTracks];
        float Track_extDB_test_point_x[nMaxTracks];
        float Track_extDB_test_point_y[nMaxTracks];
        float Track_extDB_test_point_z[nMaxTracks];
        float Track_extDB_test_first_point_x[nMaxTracks];
        float Track_extDB_test_first_point_y[nMaxTracks];
        float Track_extDB_test_first_point_z[nMaxTracks];
        float Track_extDB_test_last_point_x[nMaxTracks];
        float Track_extDB_test_last_point_y[nMaxTracks];
        float Track_extDB_test_last_point_z[nMaxTracks];
        
        float Track_extDB_PSD_track_px[nMaxTracks];
        float Track_extDB_PSD_track_py[nMaxTracks];
        float Track_extDB_PSD_track_pz[nMaxTracks];
        float Track_extDB_PSD_point_x[nMaxTracks];
        float Track_extDB_PSD_point_y[nMaxTracks];
        float Track_extDB_PSD_point_z[nMaxTracks];
        
        float Track_DBVtx_pT[nMaxTracks];
        float Track_DBVtx_px[nMaxTracks];
        float Track_DBVtx_py[nMaxTracks];
        float Track_DBVtx_pz[nMaxTracks];
        float Track_DBVtx_eta[nMaxTracks];
        float Track_DBVtx_phi[nMaxTracks];
        float Track_DBVtx_impact_point_X[nMaxTracks];
        float Track_DBVtx_impact_point_Y[nMaxTracks];
        float Track_DBVtx_impact_point_Z[nMaxTracks];
        
        float Track_RecoVtx_track_pT[nMaxTracks];
        float Track_RecoVtx_track_eta[nMaxTracks];
        float Track_RecoVtx_track_phi[nMaxTracks];
        
        float Track_RecoVtx_pT[nMaxTracks];
        float Track_RecoVtx_eta[nMaxTracks];
        float Track_RecoVtx_phi[nMaxTracks];
        float Track_RecoVtx_impact_point_X[nMaxTracks];
        float Track_RecoVtx_impact_point_Y[nMaxTracks];
        float Track_RecoVtx_impact_point_Z[nMaxTracks];
        
        int Track_DBVtx_track_charge[nMaxTracks];
        double Track_DBVtx_track_dEdx[nMaxTracks];
        int Track_DBVtx_track_nClusters_Total[nMaxTracks];
        int Track_DBVtx_track_nClusters_TPCV1[nMaxTracks];
        int Track_DBVtx_track_nClusters_TPCV2[nMaxTracks];
        int Track_DBVtx_track_nClusters_TPCVmain[nMaxTracks];
        int Track_DBVtx_track_nClusters_TPCVgap[nMaxTracks];
        int Track_DBVtx_track_nClustersPotential_Total[nMaxTracks];
        int Track_DBVtx_track_nClustersPotential_TPCV1[nMaxTracks];
        int Track_DBVtx_track_nClustersPotential_TPCV2[nMaxTracks];
        int Track_DBVtx_track_nClustersPotential_TPCVmain[nMaxTracks];
        int Track_DBVtx_track_nClustersPotential_TPCVgap[nMaxTracks];
        int Track_DBVtx_track_nClustersFit_Total[nMaxTracks];
        int Track_DBVtx_track_nClustersFit_TPCV1[nMaxTracks];
        int Track_DBVtx_track_nClustersFit_TPCV2[nMaxTracks];
        int Track_DBVtx_track_nClustersFit_TPCVmain[nMaxTracks];
        int Track_DBVtx_track_nClustersFit_TPCVgap[nMaxTracks];    
        int Track_DBVtx_track_nClustersdEdX_Total[nMaxTracks];
        int Track_DBVtx_track_nClustersdEdX_TPCV1[nMaxTracks];
        int Track_DBVtx_track_nClustersdEdX_TPCV2[nMaxTracks];
        int Track_DBVtx_track_nClustersdEdX_TPCVmain[nMaxTracks];
        int Track_DBVtx_track_nClustersdEdX_TPCVgap[nMaxTracks];
        double Track_DBVtx_track_EnergyClusters_Total[nMaxTracks];
        double Track_DBVtx_track_EnergyClusters_TPCV1[nMaxTracks];
        double Track_DBVtx_track_EnergyClusters_TPCV2[nMaxTracks];
        double Track_DBVtx_track_EnergyClusters_TPCVmain[nMaxTracks];
        double Track_DBVtx_track_EnergyClusters_TPCVgap[nMaxTracks];
        float Track_DBVtx_track_DCAtoVertex_X[nMaxTracks];
        float Track_DBVtx_track_DCAtoVertex_Y[nMaxTracks];
        float Track_DBVtx_track_DCAtoVertex_Z[nMaxTracks];
        float Track_DBVtx_track_chi2[nMaxTracks];    
        int Track_DBVtx_track_ndf[nMaxTracks];    
        
        int Track_RecoVtx_track_charge[nMaxTracks];
        double Track_RecoVtx_track_dEdx[nMaxTracks];
        int Track_RecoVtx_track_nClusters_Total[nMaxTracks];
        int Track_RecoVtx_track_nClusters_TPCV1[nMaxTracks];
        int Track_RecoVtx_track_nClusters_TPCV2[nMaxTracks];
        int Track_RecoVtx_track_nClusters_TPCVmain[nMaxTracks];
        int Track_RecoVtx_track_nClusters_TPCVgap[nMaxTracks];
        int Track_RecoVtx_track_nClustersPotential_Total[nMaxTracks];
        int Track_RecoVtx_track_nClustersPotential_TPCV1[nMaxTracks];
        int Track_RecoVtx_track_nClustersPotential_TPCV2[nMaxTracks];
        int Track_RecoVtx_track_nClustersPotential_TPCVmain[nMaxTracks];
        int Track_RecoVtx_track_nClustersPotential_TPCVgap[nMaxTracks];
        int Track_RecoVtx_track_nClustersFit_Total[nMaxTracks];
        int Track_RecoVtx_track_nClustersFit_TPCV1[nMaxTracks];
        int Track_RecoVtx_track_nClustersFit_TPCV2[nMaxTracks];
        int Track_RecoVtx_track_nClustersFit_TPCVmain[nMaxTracks];
        int Track_RecoVtx_track_nClustersFit_TPCVgap[nMaxTracks];    
        int Track_RecoVtx_track_nClustersdEdX_Total[nMaxTracks];
        int Track_RecoVtx_track_nClustersdEdX_TPCV1[nMaxTracks];
        int Track_RecoVtx_track_nClustersdEdX_TPCV2[nMaxTracks];
        int Track_RecoVtx_track_nClustersdEdX_TPCVmain[nMaxTracks];
        int Track_RecoVtx_track_nClustersdEdX_TPCVgap[nMaxTracks];
        double Track_RecoVtx_track_EnergyClusters_Total[nMaxTracks];
        double Track_RecoVtx_track_EnergyClusters_TPCV1[nMaxTracks];
        double Track_RecoVtx_track_EnergyClusters_TPCV2[nMaxTracks];
        double Track_RecoVtx_track_EnergyClusters_TPCVmain[nMaxTracks];
        double Track_RecoVtx_track_EnergyClusters_TPCVgap[nMaxTracks];
        float Track_RecoVtx_track_DCAtoVertex_X[nMaxTracks];
        float Track_RecoVtx_track_DCAtoVertex_Y[nMaxTracks];
        float Track_RecoVtx_track_DCAtoVertex_Z[nMaxTracks];
        float Track_RecoVtx_track_chi2[nMaxTracks];    
        int Track_RecoVtx_track_ndf[nMaxTracks];    
        
        int TPC_Multiplicity;
        int TPC_Multiplicity_all;
        int Primary_Multiplicity_all;
        int TPC_Multiplicity_Clusters_VTPC1_VTPC2;
        int TPC_Multiplicity_Clusters_All;
        float TPC_cos1;
        float TPC_sin1;
        float TPC_cos2;
        float TPC_sin2;
        
        //PSD
        int PSD_module_Number; //modules with energy
        int PSD_section_Number; //sections with energy
        int PSD_module_Number_array[nPSD_Modules]; //modules with energy
        int PSD_section_Number_array[nPSD_Modules][nPSD_Sections]; //sections with energy
        float PSD_section_slice_Energy[nPSD_Sections]; //sum over modules for a given section id
        float PSD_module_X[nPSD_Modules];
        float PSD_module_Y[nPSD_Modules];
        float PSD_module_Z[nPSD_Modules];
        float PSD_module_Energy[nPSD_Modules];
        int PSD_module_number_of_sections[nPSD_Modules];
        float PSD_section_Energy[nPSD_Modules][nPSD_Sections];
        float PSD_Energy;
        float PSD_1_Energy; //16+1
        float PSD_2_Energy; //12
        float PSD_3_Energy; //16
        
        //Main Vertex = DB vertex
        float Main_Vertex_X;
        float Main_Vertex_Y;
        float Main_Vertex_Z;
        
        //Primary Vertex = Reco vertex
        float Primary_Vertex_X;
        float Primary_Vertex_Y;
        float Primary_Vertex_Z;
        bool Has_Primary_Vertex;
        
        //Additionary Vertex
        
        float pVtx_Z;
        
        //Triggers    
        unsigned int BPD_Status[nRawReco][nBPD][nBPDcomponents];
        float BPD_Position[nRawReco][nBPD][nBPDcomponents];
        float BPD_PositionError[nRawReco][nBPD][nBPDcomponents];
        float BPD_Z[nRawReco][nBPD][nBPDcomponents];
        float BPD_RMS[nRawReco][nBPD][nBPDcomponents];
        float BPD_Maximum[nRawReco][nBPD][nBPDcomponents];
        float BPD_Charge[nRawReco][nBPD][nBPDcomponents];
        float BPD_SumOfAll[nRawReco][nBPD][nBPDcomponents];
        float triggersADC[nRawReco][nTriggers_Simple];
        bool isTriggers_Simple[nRawReco][nTriggers_Simple];
        bool isTriggers_Combined[nRawReco][nTriggers_Combined];
        
        //Beam
        float Beam_Momentum[nRawReco][nBeamComponents];
        float Beam_Fitted2DLineXZ[nRawReco][nBeamComponents];
        float Beam_Fitted2DLineYZ[nRawReco][nBeamComponents];
        int Beam_Status[nRawReco];;
        
        //WFA
        float WFA_TimeStructure[nTriggersWFA][nMaxWFAsignals];
        unsigned int WFA_NumberOfSignalHits[nTriggersWFA];
        

        int Primary_Vertex_Q;
        
        TFile *outputFile;
        
        REGISTER_MODULE("ConverterShineToNA61Tree", ConverterShineToNA61Tree,"$Id: ConverterShineToNA61Tree.h $");
    };
}

#endif
